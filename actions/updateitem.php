<?php
session_start();
ob_start();
include "../lib/NvooyUtils.php";

function onSuccessHandler(){
	
	include "../config/db_details.php";
	$handler = mysqli_connect( DATABASE_HOST_NAME, DATABASE_USER, DATABASE_PASSWORD, DATABASE_NAME);
	$result = mysqli_query($handler,
				sprintf("UPDATE items_table SET  
						product_name='%s',
						product_price='%s',
						product_label='%s',
						product_type ='%s',
						product_details='%s' 
						
					WHERE id='%s' AND company_id='%s';
				",
				$_POST["pname"],
				$_POST["price"],
				$_POST["plabel"],
				$_POST["ptype"],
				$_POST["pdetails"],
				$_POST["product_id"],
				$_SESSION["id"]
				)
				);
	header("Location: ../home.php?error=21");
	//SuccessFully Updated the query
	mysqli_close($handler);
	}

function onEmptyHandler(){}
function onNotSetHandler(){}

$labelArray = array(
					"pname",
					"price",
					"plabel",
					"ptype",
					"pdetails",
					"product_id"
					);

NvooyUtils::onSetAndEmptyCheckHandler($_POST, $labelArray, -1, "onSuccessHandler", "onEmptyHandler", "onNotSetHandler", true);
ob_end_flush();
?>