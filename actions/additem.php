<?php
session_start();
ob_start();
include '../lib/NvooyUtils.php';
$labelArray = array(
					"pname",
					"price",
					"plabel",
					"ptype",
					"pdetails",
					
					);
					
function generateUniqueText(){
	$time = microtime();
	$rand = 0;
	for($i=0; $i<10; ++$i){
		$rand += rand(10000, 999999999);
		}
	return substr((md5($time.$rand."UniqueSaltString")), 0, 20);
	}
					
function onSuccessHandler(){
	include "../config/db_details.php";
	$handler = mysqli_connect(DATABASE_HOST_NAME, DATABASE_USER, DATABASE_PASSWORD, DATABASE_NAME);
	
	$extarray = explode(".",$_FILES["product_pic"]["name"]);
	
	$fileName =generateUniqueText().".".$extarray[count($extarray)-1];
	
	if(isset($_FILES["product_pic"])){
		//If user has sent a product pic
		move_uploaded_file($_FILES["product_pic"]["tmp_name"], "../uploaded/".$fileName);
		}
	mysqli_query($handler,
				sprintf("INSERT INTO items_table (product_name, product_price, product_label, product_type, product_details, rating, company_id, product_pic) VALUES
				('%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s');
				",
				$_POST["pname"], $_POST["price"], $_POST["plabel"], $_POST["ptype"], $_POST["pdetails"], rand(3,5), $_SESSION["id"],$fileName
				)
				);
	if(mysqli_affected_rows($handler)){
		header("Location: ../home.php?error=12");
		//Adding Details to home successfull
		}
	mysqli_close($handler);
	}

function onEmptyHandler(){
	header("Location: ../home.php?error=11");
	//Empty Data set for registering Items
	}
function onNotSetHandler(){
	header("Location: ../home.php?error=10");
	//Not Set Handler not set error
	}

NvooyUtils::onSetAndEmptyCheckHandler($_POST, $labelArray,-1,"onSuccessHandler","onEmptyHandler","onNotSetHandler",true);
ob_end_flush();
?>