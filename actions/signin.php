<?php 


session_start();

ob_start();

if((isset($_SESSION["id"]) &&
   isset($_SESSION["user_name"]) &&
   isset($_SESSION["company_name"]))){
	//User is logged in So take him to the home page
	header("Location: home.php");
	die();
	}
	
include '../lib/NvooyUtils.php';
$labelArray = array(
					"email_id",
					"password"	
					);
					
function onSuccessHandler(){
	include '../config/db_details.php';
	$handler = mysqli_connect(DATABASE_HOST_NAME, DATABASE_USER	, DATABASE_PASSWORD, DATABASE_NAME);
	$resultCursor = mysqli_query($handler, 
						   sprintf("SELECT COUNT(id) AS count, id, user_name, company_name,profile_pic  FROM user_table WHERE email_id ='%s' AND password='%s';",$_POST['email_id'],md5($_POST['password']))
						   );
	$result = mysqli_fetch_assoc($resultCursor);
	if($result["count"] >0 ){
		//User has a valid credentials
		
		$_SESSION["id"] = $result["id"];
		$_SESSION["user_name"] = $result["user_name"];
		$_SESSION["company_name"] = $result["company_name"];
		$_SESSION["profile_pic"] = $result["profile_pic"];
		
		header("Location: ../home.php");
		
		}
	else{
		//User does not have valid credentials
		header("Location: ../signin.php?error=1");
		//Error 1 denotes wrong user name and password
		}
	mysqli_close($handler);
	}

function onEmptyHandler(){
	header("Location: ../signin.php");
	}
function onNotSetHandler(){
	header("Location: ../signin.php");
	}

NvooyUtils::onSetAndEmptyCheckHandler($_POST,
 								     $labelArray, 
									 -1 , 
									 "onSuccessHandler",
									  "onEmptyHandler",
									  "onNotSetHandler",
									  true);

	


?>		