<?php
require "../lib/NvooyUtils.php";
session_start();
ob_start();
if((isset($_SESSION["id"]) &&
	isset($_SESSION["user_name"]) &&
	isset($_SESSION["company_name"]))){
	//User is logged in So take him to the home page
	header("Location: ../home.php");
	}

$labelArray=  array(
					"uname",
					"email_id",
					"phoneNumber",
					"cname",
					"password",
					"cdetails",
					"cpname"
				);

NvooyUtils::onSetAndEmptyCheckHandler($_POST,
									 $labelArray,
									 -1,
									"onSuccessHandler",	
									"onEmptyHandler",
									"onNotSetHandler",true);
function onSuccessHandler(){
	include "../config/db_details.php";
	
	$handler = mysqli_connect(DATABASE_HOST_NAME,
							  DATABASE_USER	,
							  DATABASE_PASSWORD,
							  DATABASE_NAME );
	$extarray = explode(".",$_FILES["profile_pic"]["name"]);
	
	$fileName =generateUniqueText().".".$extarray[count($extarray)-1];
	if(isset($_FILES["profile_pic"])){
		move_uploaded_file($_FILES["profile_pic"]["tmp_name"],"../uploaded/".$fileName);
		}
	
	mysqli_query($handler, 
				sprintf("INSERT INTO user_table (user_name, email_id, company_name, company_details, account_created, password, contact_person_name, phoneNumber, profile_pic) VALUES ('%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s');",
				$_POST["uname"],
				$_POST["email_id"],
				$_POST["cname"],
				$_POST["cdetails"],	
				time(),			
				md5($_POST['password']),
				$_POST["cpname"],
				$_POST["phoneNumber"],
				$fileName
				)
				);
	if(mysqli_affected_rows($handler)){
		$_SESSION["id"] = mysqli_insert_id($handler);
		$_SESSION["user_name"] = $_POST["uname"];
		$_SESSSION["company_name"]  = $_POST["cname"];
		header("Location: ../home.php");
		}
	
				
	mysqli_close($handler);
	}

function onEmptyHandler(){
	echo "Some elements are Empty";
	}

function onNotSetHandler(){
	echo "Some elements are not set";
	}

function generateUniqueText(){
	$time = microtime();
	$rand = 0;
	for($i=0; $i<10; ++$i){
		$rand += rand(10000, 999999999);
		}
	return substr((md5($time.$rand."UniqueSaltString")), 0, 20);
	}

ob_end_flush();

?>