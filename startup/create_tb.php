<?php

include '../config/db_details.php';

$dbHandler = mysqli_connect(DATABASE_HOST_NAME,
							DATABASE_USER,
							DATABASE_PASSWORD, 
							DATABASE_NAME);
							
							
$queryArray  = array(
						"create_user_table" => "CREATE TABLE IF NOT EXISTS user_table (
						id INT NOT NULL PRIMARY KEY AUTO_INCREMENT,
						user_name VARCHAR(200) NOT NULL,
						email_id VARCHAR(200) NOT NULL UNIQUE,
						company_name VARCHAR(200),
						phoneNumber VARCHAR(200) NOT NULL UNIQUE,
						company_details TEXT,
						account_created VARCHAR(200) NOT NULL,
						password VARCHAR(200) NOT NULL,
						contact_person_name VARCHAR(200) NOT NULL,
						profile_pic TEXT
						);",
						"items_table" => "CREATE TABLE IF NOT EXISTS items_table (
						id INT NOT NULL PRIMARY KEY AUTO_INCREMENT,
						product_name TEXT NOT NULL,
						product_price VARCHAR(200) NOT NULL,
						product_label VARCHAR(200) NOT NULL,
						product_type INT(3) NOT NULL,
						product_details TEXT NOT NULL,
						product_pic_exist INT(1) NOT NULL,
						rating VARCHAR(100) NOT NULL,
						company_id INT(20) NOT NULL,
						product_pic TEXT NOT NULL
						);",
						"images_table"=> "CREATE TABLE IF NOT EXISTS images_table (
						id INT NOT NULL PRIMARY KEY AUTO_INCREMENT,
						item_type INT(1) NOT NULL,
						item_id INT(30) NOT NULL,
						image_link TEXT NOT NULL,
						image_alt TEXT 
						);"

					);
						
foreach($queryArray as $key=>$query){
		// Iterates over all the query to be exected in the mysql
		$result = mysqli_query($dbHandler, $query);
		
		if($result){
			echo "Table Created($key)<br>";
			}
		else{
			echo "Table creation failed($key) or table already exists.<br>";
			}
		
	}
						
							
mysqli_close($dbHandler);


?>