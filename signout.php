<?php
session_start();
ob_start();

unset($_SESSION["id"]);
unset($_SESSION["user_name"]);
unset($_SESSION["company_name"]);
unset($_SESSION);

header("Location: signin.php?error=3");
ob_end_flush();


?>