<?php
//Sign in here
require 'lib/NvooyUtils.php';
session_start();
ob_start();
if((isset($_SESSION["id"]) &&
   isset($_SESSION["user_name"]) &&
   isset($_SESSION["company_name"]))){
	//User is logged in So take him to the home page
	header("Location: home.php");
	die();
	}
	
	


?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Testing</title>
<script type="text/javascript" src="bootstrap/js/jquery-1.7.1.min.js"></script>
<script type="text/javascript" src="bootstrap/js/bootstrap.js"></script>
<link rel="stylesheet" type="text/css" href="bootstrap/css/bootstrap.css"/>
<link rel="stylesheet" type="text/css" href="bootstrap/css/bootstrap-responsive.css"/>
<link rel="stylesheet" type="text/css" href="style.css">
</head>
<body>
	<div class="container-fluid">
    	<div class="row-fluid top_bar" style="position:relative">
        	<div class="span3 logo">
            	<a href="index.php"><h3 style="color:#f2f2f2">Dubai Shopping</h3></a>
            </div>
        </div>
        <div class="row-fluid">
        	<div class="container">
            	<div class="span8">
					<img src="images/shopping.png" />
				</div>
                <div class="span4">
                	<div class="login_wrapper">
                    	<div class="login_title">Login</div>
                        <div id="login_wrapper">
                            <form action="actions/signin.php" method="post" id="login">
                              <input name="email_id" type="text" size="40" placeholder="Email Id" id="username"/>
                              <input name="password" type="password" size="40" placeholder="password" id="password" />
                              <div style="clear:both"></div>
                              <div class="login_btn">
                                <button class="button" type="submit" id="">Login</button>
                            </div>
                            </form>               
                        </div>
                    </div>
                </div>
            </div>
        </div>
	</div>
<script type="text/javascript" src="js/script.js"></script>
</body>
</html>



<?php
ob_end_flush();
?>
