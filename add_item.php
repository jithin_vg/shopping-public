<?php
session_start();
ob_start();

if(!(isset($_SESSION["id"]) &&
   isset($_SESSION["user_name"]) &&
   isset($_SESSION["company_name"]))){
	//User is logged in So take him to the home page
	header("Location: signin.php?error=2");
	//Enter the user Credentials and then go forward.
	die();
	}
include "config/db_details.php";
$handler = mysqli_connect(DATABASE_HOST_NAME,DATABASE_USER,DATABASE_PASSWORD,DATABASE_NAME );
if(isset($_GET["search"])){
	if(!empty($_GET["search"])){
		if($_GET["search"] == "all"){
		$resultCursor= mysqli_query($handler, 
			  sprintf("SELECT * FROM items_table WHERE company_id= '%s';", $_SESSION["id"]));
			
			
			}
		else{
	$resultCursor = mysqli_query($handler,
									"SELECT * FROM items_table WHERE company_id='{$_SESSION["id"]}' AND ( product_details LIKE '%{$_GET["search"]}' OR product_name LIKE '%{$_GET["search"]}%' );");	
		}
		}
	
	}
else{
	$resultCursor= mysqli_query($handler, 
			  sprintf("SELECT * FROM items_table WHERE company_id= '%s';", $_SESSION["id"])
			  );
}
	


?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Testing</title>
<script type="text/javascript" src="bootstrap/js/jquery-1.7.1.min.js"></script>
<script type="text/javascript" src="bootstrap/js/bootstrap.js"></script>
<link rel="stylesheet" type="text/css" href="bootstrap/css/bootstrap.css"/>
<link rel="stylesheet" type="text/css" href="bootstrap/css/bootstrap-responsive.css"/>
<link rel="stylesheet" type="text/css" href="style.css">

</head>
<body>
	<div class="container-fluid">
    	<div class="row-fluid top_bar">
        	<div class="span3 logo">
            	<div style="color:#f2f2f2; width:200px"><a href="index.php"><img src="images/dubai_logo.png" style=width:100px"/></a></div>
            </div>
            <div class="span4">
            	<div id="search_wrapper" style="margin:13px 10px 5px;">
                            <form method="get" action="" id="search" style="margin:0px;">
                              <input name="search" class="search" type="text" size="40" placeholder="Search..." />
                            </form>               
                        </div>
            </div>
            <div class="span2 offset3">
                <div class="user-wrapper">
                    <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                    <div>
                    <div class="profile-pic">
                    <?php
					  $extn = explode(".",$_SESSION["profile_pic"]);
					  if(empty($extn[count($extn)-1])){
						  $index = false;
						  }
						  else{
							  $index = true;
							  }
							
					?>
                      <img src="<?php 
					   echo ((!$index)?"images/boyicon.gif":("uploaded/".$_SESSION["profile_pic"])); 
					   ?>" width="36" height="37" />
                    </div>
                    <div class="user">
                        <div class="username"><?php echo $_SESSION["user_name"]; ?></div>
                    </div>
                    <div style="clear:both"></div></div>
                    </a>
                <!--<div class="logout">
                	<a href="#"><img src="images/Logout-icon.png" width="24" height="24" /></a>
                </div>--> 
                    <ul class="dropdown-menu">                     
                      <li><a href="signout.php">logout</a></li>
                      <li><a href="#">Settings</a></li> 
                      <li class="divider"></li>
                      <li><a href="#">About us</a></li>
                    </ul>
           		</div>
            </div>
        </div>
        <div class="row-fluid">
        	<div class="span3 sidebar">
            	<div class="row-fluid">
                	<div class="span12 home_link">
                    `	<ul>
                    		<a href="add_item.php"><li>Add product</li></a>
                            <a href="home.php"><li>View product</li></a>
                            <li>Settings</li>
                    	</ul>
                    </div> 

                </div>
            </div>
            <div class="span9" style="margin-left: 23.076923076923077%; margin-top: 60px;">
            	<div class="row-fluid">
                	<div class="span12" style="padding:40px 60px;" id="add_form">
                        <h2>Add a new Item</h2><hr />
                        <form action="actions/additem.php" method="post" enctype="multipart/form-data">
                            <input type="text" placeholder="Product name" name="pname" /><br />
                            <input type="text" placeholder="Product Price" name="price" /><br />
                            <input type="text" placeholder="Product Label" name="plabel" /><br />
                            <input type="text" placeholder="Product type"  name="ptype" /> <br />
                            <textarea cols="16" rows="6" name="pdetails">Product Details</textarea><br />
                            <input type="file" name="product_pic" value="Upload Product Image" />
                            <br /><br />
                            <input type="submit" name="submit" value="Submit" class="button" style="width:150px;" />
                        </form>
                    </div>
            	</div>
            </div>
         </div>

</div>
</body>
</html>
<?php
mysqli_close($handler);
ob_end_flush();
?>