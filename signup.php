<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Testing</title>
<script type="text/javascript" src="bootstrap/js/jquery-1.7.1.min.js"></script>
<script type="text/javascript" src="bootstrap/js/bootstrap.js"></script>
<link rel="stylesheet" type="text/css" href="bootstrap/css/bootstrap.css"/>
<link rel="stylesheet" type="text/css" href="bootstrap/css/bootstrap-responsive.css"/>
<link rel="stylesheet" type="text/css" href="style.css">
</head>
<body>
	<div class="container-fluid">
    	<div class="row-fluid top_bar" style="position:relative">
        	<div class="span3 logo">
            	<a href="index.php"><h3 style="color:#f2f2f2">Dubai Shopping</h3></a>
            </div>
        </div>
        <div style="width:300px; margin:40px auto;" id="add_form">
        	<h3>Sign Up</h3>
            <form action="actions/signup.php" method="post" enctype="multipart/form-data">
                <input type="text" placeholder="username" name="uname" /><br />
                <input type="text" placeholder="email id" name="email_id" /><br />
                <input type="text" placeholder="Phone Number" name="phoneNumber"  /><br />
                <input type="text" placeholder="company name" name="cname"/><br />
                <input type="text" placeholder="Contact person name" name="cpname" /><br />
                <input type="password" placeholder="password" name="password" /> <br /><br />
                Company Details<br />
                <textarea name="cdetails" rows="6" cols="17"></textarea>
                <br />
                <input type="file" name="profile_pic" value="Upload Profile Picture" />
                <br /><br />
                <input type="submit" value="Submit" class="button" style="width:150px;" />
            
            </form>
         </div>
      </div>
</body>
</html>