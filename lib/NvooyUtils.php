<?php


class NvooyUtils {

    public static function nvooy_isset($ArrayToBeChecked, $TargetArray){
        //Check If all the Elements in the Array is set
        //RETURNS TRUE IF EVERY ELEMENT IS TRUE
        //RETURNS AN ARRAY WITH ALL THE ELEMENTS WHICH ARE NOT SET
        /*
         * Profiling Details with xdebug
         * ----------------------------------------------------
         * Execution time => 0.8-1.3 ms when tried with 5 Elements in Target Array and
         * 3 Elements on ArrayToBeChecked, and when Elements are all set
         * Execution time => 1.7-2.0 ms when tried with 5 Elements in TargetArray and
         * 4 Elements on the ArrayToBeChecked, and when 1 Element is not Set.
         *
         * */

        $Boolean_Element_Set_Flag = true;
        $NotSetElementsArray = array();

        foreach($TargetArray as $ToBeCheckedElement){
            //Check if all the Elements Are Set
            if(!isset($ArrayToBeChecked[$ToBeCheckedElement])){
                $Boolean_Element_Set_Flag = false;
                array_push($NotSetElementsArray,$ToBeCheckedElement);
            }
        }

        return (($Boolean_Element_Set_Flag==true)?true:$NotSetElementsArray);

    }

    public static function nvooy_not_empty($ArrayToBeChecked,$ArrayofElementsWhichisToBeCheckedEmpty,$handleSpace){
        /*
         * Checks to See if Every Array in the Element is not Empty
         * Returns TRUE if every Elements are not Empty
         * Returns the Array With Empty Elements if Some of them are Empty
         *
         * of Comparision Array is -1 then all the value are checked for Empty
         * */
        $Boolean_Element_Empty_Flag = true;
        $EmptyElementsArray = array();
       if(is_array($ArrayofElementsWhichisToBeCheckedEmpty)){
           foreach($ArrayofElementsWhichisToBeCheckedEmpty as $key=>$Element){

               if(empty($ArrayToBeChecked[$Element])){
                   $Boolean_Element_Empty_Flag=false;
                  array_push($EmptyElementsArray, $key);
               }
               else if($handleSpace){
                   if(strlen(trim($Element))==0){
                       $Boolean_Element_Empty_Flag=false;
                       array_push($EmptyElementsArray, $key);
                   }
               }
           }
       }
        else if($ArrayofElementsWhichisToBeCheckedEmpty == -1){
            /*
             * -1 denotes Check all the Elements
             * */
            foreach($ArrayToBeChecked as $key=>$Element){

                if(empty($Element)){
                    $Boolean_Element_Empty_Flag = false;
                    array_push($EmptyElementsArray, $key);
                }
                else if($handleSpace){
                    if(strlen(trim($Element))==0){
                        $Boolean_Element_Empty_Flag=false;
                        array_push($EmptyElementsArray, $key);
                    }
                }
            }
        }
        else{
            return false;
            //If the Both of the Condition are false then the function returns a false
            //To indicate that its wrong.
        }

        return (($Boolean_Element_Empty_Flag==true)?true:$EmptyElementsArray);
        //Returns the Element
    }



    public static function onSetAndEmptyCheckHandler($TargetArray, $labelArray,$emptyCheckArray,
                                                     $onSuccessHandler, $onEmptyHandler, $onNotSetHandler,
                                                     $handleSpace=0
                                                     ){

        /*
         * This Function Accepts 5 Different Arguements
         *          $TargetArray -> Array That is to be Analysed (USUAL POST, GET Array)
         *          $labelArray  -> Array Which is passed to Compare for the Array to be Analysed.
         *                          ( It should contain all the required Arguements)
         *          $emptyCheckArray -> Empty check array contains all the elements which are to checked empty against
         *                           the TargetArray, if -1 then all the elements are checked.
         *          EVENT HANDLERS
         *          ---------------------------------------------------------------------------------------------------
         *          $onsuccessHandler -> This function is called when Everything Goes All rights
         *          $onEmptyHandler   -> This function is called when Some Element is empty-> Element names are also passed
         *          $onNotSetHandler  -> This function is called when Some Element is not set -> Element name is passed to
         *                               the Handler function.
         *          $handleSpace   -> If Just space should be checked.
         *
         * */

        $isset = NvooyUtils::nvooy_isset($TargetArray,$labelArray);
        //Array with Variable Names which are Not Set
        if(is_array($isset)){
            //Some Variables were Not Set
            $onNotSetHandler($isset);
            //User Specified Function is called When Some Element is Not set
        }
        else{
            //Variables were set
            $is_empty = NvooyUtils::nvooy_not_empty($TargetArray,$emptyCheckArray,$handleSpace);
            if(is_array($is_empty)){
                //If some Element of this Array is Empty
                $onEmptyHandler($is_empty);
                //When Some Element of this Array is Empty
            }
            else{
                //If None of the Elements in array has an Empty Value
                $onSuccessHandler();
                //When The Elements of the array are not empty and when

            }
        }}


	
    public  static function nvooy_person_tag_check($TagString){
        //Check if the String Inputted is of THe person Tag

        if(preg_match_all("/[@][a-zA-Z0-9_\.\-]+/",$TagString,$match_array)){
            //If some Variable Matches.

            if(sizeof($match_array[0])>1){
                return false;
            }
            else if(strlen($match_array[0][0]) == strlen(trim($TagString))){
                return true;
            }
        }
        return false;
    }

    /*
     * Check if the email id passed or the array of email ids Have the right format
     * @param String|Array --> Either the Email id or an array which has all the email ids
     * @return Boolean|Int Boolean true if every element(s) matches the test or an integer with the Position of the Wrong email id
     * */
    public static function isEmailId($email){
        return (filter_var($email,FILTER_VALIDATE_EMAIL)?true:false);
    }

}



?>