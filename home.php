<?php
session_start();
ob_start();

if(!(isset($_SESSION["id"]) &&
   isset($_SESSION["user_name"]) &&
   isset($_SESSION["company_name"]))){
	//User is logged in So take him to the home page
	header("Location: signin.php?error=2");
	//Enter the user Credentials and then go forward.
	die();
	}
include "config/db_details.php";
$handler = mysqli_connect(DATABASE_HOST_NAME,DATABASE_USER,DATABASE_PASSWORD,DATABASE_NAME );
if(isset($_GET["search"])){
	if(!empty($_GET["search"])){
		if($_GET["search"] == "all"){
		$resultCursor= mysqli_query($handler, 
			  sprintf("SELECT * FROM items_table WHERE company_id= '%s';", $_SESSION["id"]));
			
			
			}
		else{
	$resultCursor = mysqli_query($handler,
									"SELECT * FROM items_table WHERE company_id='{$_SESSION["id"]}' AND ( product_details LIKE '%{$_GET["search"]}' OR product_name LIKE '%{$_GET["search"]}%' );");	
		}
		}
	
	}
else{
	$resultCursor= mysqli_query($handler, 
			  sprintf("SELECT * FROM items_table WHERE company_id= '%s';", $_SESSION["id"])
			  );
}
	


?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Testing</title>
<script type="text/javascript" src="bootstrap/js/jquery-1.7.1.min.js"></script>
<script type="text/javascript" src="bootstrap/js/bootstrap.js"></script>
<link rel="stylesheet" type="text/css" href="bootstrap/css/bootstrap.css"/>
<link rel="stylesheet" type="text/css" href="bootstrap/css/bootstrap-responsive.css"/>
<link rel="stylesheet" type="text/css" href="style.css">

</head>
<body>
	<div class="container-fluid">
    	<div class="row-fluid top_bar">
        	<div class="span3 logo">
            	<div style="color:#f2f2f2; width:200px"><a href="index.php"><img src="images/dubai_logo.png" style=width:100px"/></a></div>
            </div>
            <div class="span4">
            	<div id="search_wrapper" style="margin:13px 10px 5px;">
                            <form method="get" action="" id="search" style="margin:0px;">
                              <input name="search" class="search" type="text" size="40" placeholder="Search..." />
                            </form>               
                        </div>
            </div>
            <div class="span2 offset3">
                <div class="user-wrapper">
                    <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                    <div>
                    <div class="profile-pic">
                    <?php
					  $extn = explode(".",$_SESSION["profile_pic"]);
					  if(empty($extn[count($extn)-1])){
						  $index = false;
						  }
						  else{
							  $index = true;
							  }
							
					?>
                      <img src="<?php 
					   echo ((!$index)?"images/boyicon.gif":("uploaded/".$_SESSION["profile_pic"])); 
					   ?>" width="36" height="37" />
                    </div>
                    <div class="user">
                        <div class="username"><?php echo $_SESSION["user_name"]; ?></div>
                    </div>
                    <div style="clear:both"></div></div>
                    </a>
                <!--<div class="logout">
                	<a href="#"><img src="images/Logout-icon.png" width="24" height="24" /></a>
                </div>--> 
                    <ul class="dropdown-menu">                     
                      <li><a href="signout.php">logout</a></li>
                      <li><a href="#">Settings</a></li> 
                      <li class="divider"></li>
                      <li><a href="#">About us</a></li>
                    </ul>
           		</div>
            </div>
        </div>
        <div class="row-fluid">
        	<div class="span3 sidebar">
            	<div class="row-fluid">
                	<div class="span12 home_link">
                    `	<ul>
                    		<a href="add_item.php"><li>Add product</li></a>
                            <a href="home.php"><li>View product</li></a>
                            <li>Settings</li>
                    	</ul>
                    </div> 

                </div>
            </div>
            <div class="span9" style="margin-left: 23.076923076923077%; margin-top: 60px;">
            	<div class="row-fluid">
                	<div class="span12" style="">
                        <div class="product_holder">
                            <ul>
								<?php
								if(mysqli_affected_rows($handler)){
									echo "<h3 class='product_text'>Products(".mysqli_num_rows($resultCursor).")</h3><hr>";
									while($array = mysqli_fetch_assoc($resultCursor)){
										
								?>
                                <a href="edit_item.php?product_id=<?php echo $array["id"]; ?>">
                                <li class="product_wrapper_li">
                                    <div class="product_wrapper">
                                        <div class="product_image"><img src="<?php echo ((!empty($array["product_pic"]))?"uploaded/".$array["product_pic"]:"images/3.png"); ?>" /></div>
                                        <div class="product_details_wrapper">
                                            <div class="product_name"><?php echo $array["product_name"]; ?></div>
                                            <div class="product_price"><?php echo "Price: ".$array["product_price"]; ?></div>
                                            <div class="product_cat"><?php echo $array["product_label"]; ?></div>
                                            <div class="product_descr"><?php echo $array["product_details"]; ?></div>
                                        </div>
                                    </div>
                                     <div style="clear:both"></div>
                                </li>
                                </a>
                                    
								<?php
                                    }
                                }
                                
                                ?>
                                
                            </ul>
                            <div style="clear:both"></div>
                        </div>
                    </div>
            </div>
        </div>
    </div>
</div>
<div id="light" class="white_content">
	<div class="detail_wrapper">
    	<div class="detail_gallery">
   	    	<img src="images/detail1.png" /> 
        </div>
        <div class="details_right_wrapper">
        	<div class="detail_item_title">Mens Crew Neck</div>
            <div class="detail_item_price">$45</div>
            <div class="detail_item_descrp">With more style than your typical crewneck, the Squad Life Sweatshirt by Crooks and Castles features contrast bandanna print about the shoulders and neckline, as well as a zipper pocket detail at the left side. Rock this style savvy sweatshirt with dark denim and high tops to showcase your fashion sense.</div>
            <div class="detail_addCart">
            	<button class="button" type="button" id="">Add to Cart</button>
            </div>
            <div class="detail_item_more">
                <div class="tabbable" style="margin-bottom: 18px;">
                  <ul class="nav nav-tabs">
                    <li class="active"><a href="#tab1" data-toggle="tab">Section 1</a></li>
                    <li class=""><a href="#tab2" data-toggle="tab">Section 2</a></li>
                    <li class=""><a href="#tab3" data-toggle="tab">Section 3</a></li>
                  </ul>
                  <div class="tab-content" style="padding-bottom: 9px;">
                    <div class="tab-pane active" id="tab1">
                      <p>I'm in Section 1.</p>
                    </div>
                    <div class="tab-pane" id="tab2">
                      <p>Howdy, I'm in Section 2.</p>
                    </div>
                    <div class="tab-pane" id="tab3">
                      <p>What up girl, this is Section 3.</p>
                    </div>
                  </div>
                </div>            
            </div>
        </div>
        <div style="clear:both"></div>
    </div>
</div>
<div id="fade" class="black_overlay" onclick = "document.getElementById('light').style.display='none';document.getElementById('fade').style.display='none'"></div>
<script type="text/javascript" src="js/script.js"></script>
</body>
</html>


<?php
mysqli_close($handler);
ob_end_flush();
?>